import {Component, OnInit} from "@angular/core"
import {LoginService} from "./login.service";
import {Router} from "@angular/router";
import {Autenticao} from "../config/autenticao.model";


@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit{

  private email: string
  private senha: string

  constructor(
    private loginService: LoginService,
    private router: Router,
    private autenticacao: Autenticao
  ){

  }

  ngOnInit() {
      if(this.autenticacao.isLogado()){
        this.router.navigate(['/empresas'])
      }
  }

  login(){
    this.loginService.signIn(this.email, this.senha)
      .subscribe(res => {
        if (res.json().success){
          this.autenticacao.setLogado()
          this.autenticacao.setDados(res.headers.get('access-token'), res.headers.get('uid'), res.headers.get('client'))
        } else {
          alert("Erro ao logar")
        }
      })
    if(this.autenticacao.isLogado()){
      this.router.navigate(['/empresas'])
    }
  }

}
