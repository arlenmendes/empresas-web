import { Injectable} from "@angular/core"
import { Headers, Http, RequestOptions} from "@angular/http";
import {config } from "../config/configuracao"

import { Autenticao } from "../config/autenticao.model"
import {HttpHeaders} from "@angular/common/http";

@Injectable()
export class LoginService {
  // private uid
  // private token
  // private cliente
  // private logado: boolean = false

  constructor(
    private http: Http,
    private autenticacao: Autenticao
  ){

  }

  signIn(email: string, senha: string){
    const headers = new Headers({'Content-Type': 'application/json'})
    const options = new RequestOptions({headers: headers})

    const dados = {'email': email, 'password': senha}
    return this.http.post(
      config.host + ':' + config.porta + '/api/' + config.versao + '/users/auth/sign_in',
      dados,
      {
        // headers: new HttpHeaders().set('Content-Type', 'application/json')
        headers: headers
      }
    )
  }

  // getUid() { return this.uid }
  // getTOken() { return this.token }
  // getCliente() { return this.cliente }
  // public isLogado() { return this.logado }
}
