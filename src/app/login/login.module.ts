import {NgModule} from "@angular/core"
import {LoginComponent} from "./login.component"
import { FormsModule } from "@angular/forms"
import {LoginService} from "./login.service"
import {Http} from "@angular/http"


@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    FormsModule

  ],
  providers: [
    LoginService,
    Http,
  ]
})

export class LoginModule{

}
