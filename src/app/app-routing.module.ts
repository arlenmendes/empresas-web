import {NgModule} from "@angular/core"
import {RouterModule, Routes} from "@angular/router"
import {LoginComponent} from "./login/login.component";
import {EmpresasComponent} from "./empresas/empresas.component";


const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'empresas', component: EmpresasComponent},
  {path: '', redirectTo: '/login', pathMatch: 'full'}
]

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
