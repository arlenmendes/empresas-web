import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import {HttpModule} from "@angular/http"

//Módulos da aplicação
import { AppRoutingModule } from "./app-routing.module"
import { LoginModule } from "./login/login.module"
import { EmpresasModule } from "./empresas/empresas.module"

//Classes da Aplicação
import { Autenticao } from "./config/autenticao.model"

//Componentes da aplicação
import { AppComponent } from './app.component'

@NgModule({
  declarations: [
    AppComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    EmpresasModule,
    HttpModule,

  ],
  providers: [
    Autenticao
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
