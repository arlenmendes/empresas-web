import {Component, Input, OnInit} from "@angular/core"
import {Empresa} from "../empresa.model"
import {Router} from "@angular/router";
import {EmpresasService} from "../empresas.service";

@Component({
  selector: 'empresas-lista',
  templateUrl: './empresas-lista.component.html',
  styleUrls: ['./empresas-lista.component.css'],
})

export class EmpresasListaComponent implements OnInit{

  @Input() empresas: Empresa[]
  private empresa
  private ahEmpresaDetalhe: boolean
  @Input() listar


  constructor(
    private router: Router,
    private empresasService: EmpresasService
  ){}
  ngOnInit(){
    this.ahEmpresaDetalhe = false
  }
  empresaDetalhe(id){
      this.empresasService.buscarPorId(id).subscribe(
        res =>{
          this.empresa = res.json().enterprise
        }
      )
      this.ahEmpresaDetalhe = true
  }
}
