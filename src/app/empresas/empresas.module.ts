import {NgModule} from "@angular/core"
import {EmpresasComponent} from "./empresas.component"
import {Http} from "@angular/http"
import { EmpresasService } from "./empresas.service"
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {EmpresasListaComponent} from "./empresas-lista/empresas-lista.component";
import {EmpresasDetalheComponent} from "./empresas-detalhe/empresas-detalhe.component";
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
  {path: 'empresas/:id', component: EmpresasDetalheComponent}
]

@NgModule({
  declarations: [
    EmpresasComponent,
    EmpresasListaComponent,
    EmpresasDetalheComponent,
  ],
  providers: [
    EmpresasService,
    Http
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class EmpresasModule {}
