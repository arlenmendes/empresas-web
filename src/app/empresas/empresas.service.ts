import {Injectable} from "@angular/core"
import {Http, URLSearchParams, RequestOptions} from "@angular/http"
import {Autenticao} from "../config/autenticao.model"
import { config } from "../config/configuracao"
import {Empresa} from "./empresa.model";

@Injectable()
export class EmpresasService {
  private empresaDetalhe: Empresa
  constructor(
    private http: Http,
    private autenticacao: Autenticao,
  ){
  }

  buscar(filtro) {
    let params = new URLSearchParams()
    params.set('name', filtro)

    const options = new RequestOptions({headers: this.autenticacao.getHeader(), params: params})
    return this.http.get(config.host + ":" + config.porta + "/api/" + config.versao + "/enterprises",options)
  }

  buscarPorId(id){
      const options = new RequestOptions({headers: this.autenticacao.getHeader()})
      return this.http.get(config.host + ":" + config.porta + "/api/" + config.versao + "/enterprises/" + id, options)
  }
}
