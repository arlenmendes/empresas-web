import {Component, OnInit} from "@angular/core"
import {Empresa} from "./empresa.model";
import {EmpresasService} from "./empresas.service";
import {Autenticao} from "../config/autenticao.model";
import {Router} from "@angular/router";

@Component({
  styleUrls: ['./empresas.component.css'],
  templateUrl: './empresas.component.html'
})

export class EmpresasComponent implements OnInit{

  dados = [
    {
      'enterprise_name': 'Empresa 1',
      'enterprise_type': {'enterprise_type_name': 'Comida'},
      'country': 'Brasil'
    },
    {
      'enterprise_name': 'Empresa 2',
      'enterprise_type': {'enterprise_type_name': 'Carros'},
      'country': 'Espanha'
    }
  ]

  private empresaDetalhe = Empresa
  private empresas = this.dados
  private buscando = false
  private ahEmpresas = false
  private valor
  private listar = true

  constructor(
    private empresasService: EmpresasService,
    private autenticacao: Autenticao,
    private router: Router
  ){}

  ngOnInit() {
      if (!this.autenticacao.isLogado()){
        this.router.navigate(['/login'])
      }
  }

  setBuscando(){
    this.buscando = !this.buscando
  }

  buscarEmpresas(d){
    this.empresasService.buscar(d).subscribe(
      res => {
        this.empresas = res.json().enterprises
        this.ahEmpresas = true
      }
    )
    this.listar = true
  }

}
