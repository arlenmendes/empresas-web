import { Headers } from "@angular/http"

export class Autenticao {
  private token: string
  private uid: string
  private client: string
  private logado: boolean = false


  public isLogado(){
    return this.logado
  }

  public setLogado(){
    this.logado = true
  }

  public setDados(token: string, uid: string, client: string) {
    this.token = token
    this.uid = uid
    this.client = client
  }

  public getHeader(): Headers{
    return new Headers(
      {
        'Content-type': 'application/json',
        'access-token': this.token,
        'client': this.client,
        'uid': this.uid
      }
    )
  }
}
